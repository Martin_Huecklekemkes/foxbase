# ToDo

## Tasks

- [x] create Angular project
- [x] create components
- [x] basic HTML
- [x] basic styling
- [x] data structure
- [x] example data
&nbsp;

## architecture

- [ ] components
  - [x] content
  - [x] header
  - [x] card
- [ ] services
  - [ ] fetch data
- [ ] interfaces
  - [x] nodes

```mermaid
flowchart TD
A[data] -- provides data --> B
B[processing] -- delivers data --> C
B[processing] -- knows where to get data --> A
C[presentation] -- knows who to ask for data --> B
```

## basic functionality

### Milestone 1

- [x] fetch data from data.json
- [x] get node title into header
- [x] get node description into card
- [x] load all nodes from same level

### Milestone 2

- [ ] make card clickable
- [ ] load new card content
- [ ] save decisions

## advanced functionality

- [ ] incompabilities
- [ ] conditionals

## more choice options

- [ ] binary choice
- [ ] checkbox
- [ ] slider
