import { Node, siteType } from './node';

export const NODES: Node[] = [
  {
    node: 0,
    level: 0,
    data: {
      title: 'Title',
      description: 'Description',
    },
    siteType: siteType.decision,
    incompabilities: [0],
    childNodes: [0],
    parentNodes: [0],
    nextDecisionNode: 0,
  },
  {
    node: 1,
    level: 1,
    data: {
      title: 'Wählen sie ihre Anwendugsbereich',
      description: 'In welchem Bereich möchten Sie die Farbe anwenden?',
    },
    siteType: siteType.decision,
    incompabilities: [],
    childNodes: [2, 3],
    parentNodes: [],
    nextDecisionNode: 4,
  },
  {
    node: 2,
    level: 2,
    data: {
      title: 'Außenbereich',
      description: 'This is a description for Außenbereich',
    },
    siteType: siteType.card,
    incompabilities: [0],
    childNodes: [4],
    parentNodes: [1],
  },
  {
    node: 3,
    level: 2,
    data: {
      title: 'Innenbereich',
      description: 'This is a description for Innenbereich',
    },
    siteType: siteType.card,
    incompabilities: [6],
    childNodes: [4],
    parentNodes: [1],
    nextDecisionNode: 4,
  },
  {
    node: 4,
    level: 3,
    data: {
      title: 'Untergrund',
      description: 'Auf welchem Untergrund soll die Farbe aufgetragen werden?',
    },
    siteType: siteType.decision,
    incompabilities: [0],
    childNodes: [5, 6, 7, 8],
    parentNodes: [2, 3],
    nextDecisionNode: 7,
  },
];
