import { Injectable } from '@angular/core';

import { Node } from '../node';
import { NODES } from '../mockdata';

const STARTINGNODE = 1;
let nextNode: number = 1;

@Injectable({
  providedIn: 'root',
})
export class NodesService {
  constructor() {}

  getDecisionNode(currentNodeID = STARTINGNODE): Node {
    console.log(NODES[currentNodeID]);
    let currentNode = NODES[currentNodeID];
    nextNode = currentNode.nextDecisionNode!;
    console.log(nextNode);
    console.log('getChildNodes');

    return NODES[currentNodeID];
  }

  getChildNodes(decisionNodeID: number): Node[] {
    let childNodes: Node[] = [];
    let childNodeIDs = NODES[decisionNodeID].childNodes;
    console.log(NODES[decisionNodeID].childNodes);

    for (let childNodeID of childNodeIDs!) {
      childNodes.push(NODES[childNodeID]);
    }
    console.log(childNodes);
    return childNodes
  }
}
