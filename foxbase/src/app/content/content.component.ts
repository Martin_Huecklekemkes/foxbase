import { Component, OnInit } from '@angular/core';

import { NodesService } from '../services/nodes.service';
import { Node } from '../node';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
})
export class ContentComponent implements OnInit {
  constructor(private nodeService: NodesService) {}

  nodes: Node[] = [];
  decisionNode?: Node;
  childNodes?: Node[];

  ngOnInit(): void {
    this.getDecisionNode();
    this.getChildNodes();
  }

  getDecisionNode() {
    this.decisionNode = this.nodeService.getDecisionNode();
  }

  getChildNodes(){
    this.childNodes = this.nodeService.getChildNodes(1);
  }

  
}
