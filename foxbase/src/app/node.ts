
export interface Node {
  node: number;
  level: number;
  data: {
    title: string;
    description: string;
  };
  siteType: siteType;
  incompabilities?: number[];
  childNodes?: number[];
  parentNodes?: number[];
  nextDecisionNode?: number;
}

export enum siteType {
  decision,
  card,
  checkboxChoice,
  binaryChoice,
}